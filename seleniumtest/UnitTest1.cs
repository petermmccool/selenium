﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;

namespace seleniumtest
{
    [TestClass]
    public class UnitTest1
    {
        private FirefoxDriver _firefoxDriver = new FirefoxDriver();
        [TestMethod]
        public void TestMainPage()
        {
            _firefoxDriver.Navigate().GoToUrl("http://localhost:3000/");
            _firefoxDriver.FindElement(By.ClassName("main-app-container"));   
        }

        [TestMethod]
        public void TestServicesPage()
        {
            _firefoxDriver.Navigate().GoToUrl("http://localhost:3000/#/app/services");
            _firefoxDriver.FindElementByXPath("//*[contains(text(), 'AUPER-P-APSN02')]");
        }

        [TestCleanup]
        public void TearDown()
        {
            _firefoxDriver.Close();
        }
    }
}
